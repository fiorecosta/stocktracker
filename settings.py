class Settings():
    """A class to store all settings for PyTicker"""

    def __init__(self):
        """Initialise the programs settings."""
        self.url = "https://api.iextrading.com/1.0/stock"
        self.interval = "5min"
        self.chartInt = "1m"
        #self.tick = "AAPL"