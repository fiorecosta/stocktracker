# mod. for Messenger functions
import json

def stockInfo(data):
    print("")
    print("--------------  Stock Info ------------------------\n")
    print("Symbol        :  {}".format(data["symbol"]))
    print("Company Name  :  {}".format(data["companyName"]))
    print("Sector        :  {}".format(data["sector"]))
    print("Market Cap.   :  {}".format(data["marketCap"]))
    print("PE Ratio      :  {}".format(data["peRatio"]))
    print("52w High      :  {}".format(data["week52High"]))
    print("52w Low       :  {}".format(data["week52Low"]))
    print("YTD Change    :  {}".format(data["ytdChange"]))
    print("\n----------------------------------------------------\n")

def verI():
    print("\n\n")
    print("-- APP INFO --------")
    print("   Version 1.0")
    print("   Written by Costa")
    print("   17/11/2018")
    print("--------------------")

