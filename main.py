# Python Program for XYZ written by Costa

"""Import Modules and Settings"""
from settings import Settings
import json
import requests
import messenger

"""Accociate settings"""
mSet = Settings()
msg = messenger

#chartURL = requests.get(chartURL)

def menu():
    print("- Enter Choice -")
    print("- Info   = i   -")
    print("----------------")
    mIn = input("\n Enter Choice: >>  ")
    if mIn == "t":
        tick = input("Enter Stock Market Ticker >> ")

    elif mIn == "i":

        tick = input("Enter Stock Market Ticker  >> ")

        """ Define URLs for data sets """
        infoURL = ("{0}/{1}/quote".format(mSet.url, tick))  # Base Stock Info
        chartURL = ("{0}/{1}/chart/{2}".format(mSet.url, tick, mSet.chartInt))  # Chart Info

        """Debug URLs"""
        # print(infoURL)
        # print(chartURL)

        """Save json file from URLs"""
        infoReq = requests.get(infoURL)
        infoJson = infoReq.json()

        """ run stock info """
        msg.stockInfo(infoJson)
        return True

    elif mIn == "e":
        msg.verI()
        print("\n\n ##  EXIT  ##")
        return False
    else:
        print("#  WRONG INPUT  #")
        return True

# Set State to True (exit switcher)
state = True

while state:
    state = menu()

#TODO trycatch exceptions for requests: http://docs.python-requests.org/en/master/user/quickstart/#errors-and-exceptions